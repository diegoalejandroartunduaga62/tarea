"""
URL configuration for tareas project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from appTareas import viewss
import appTareas
from rest_framework.authtoken import views
from django.views.generic import RedirectView
from rest_framework.routers import DefaultRouter
router=DefaultRouter()
router.register(r'tareas',viewss.TareaViewSet,basename="tareas"),
router.register(r'tareas/edit', viewss.TareaViewEdit, basename="tarea edit")

urlpatterns = [    
    #api
    # path('', RedirectView.as_view(pattern_name='login_view', permanent=False)),
    path('apis/',include(router.urls)),
    path('api-token-auth/',views.obtain_auth_token),
    path('admin/', admin.site.urls),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('api/', include('appTareas.urls')),
    
    
    #templates
    path('login_view/', appTareas.viewss.login_view, name='login_view'),
    path('tarea-lista/', viewss.tarea_list_view, name='tarea-lista'),
    path('tarea-lista-complete/', viewss.tarea_list_complete_view, name='tarea-lista-complete'),
    path('tarea_form/', viewss.tarea_form_view, name='tarea_form'),
    path('cambiar-completada/<int:tarea_id>/', viewss.cambiar_completada, name='cambiar-completada'),
    path('editar-tarea/<int:tarea_id>/', viewss.editar_tarea, name='editar_tarea'),
    path('eliminar-tarea/<int:tarea_id>/', viewss.eliminar_tarea, name='eliminar_tarea'),
    path('logout/', viewss.custom_logout_view, name='logout'),
    
]


