from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.db import models
from django.utils import timezone
from django.db.models.signals import post_migrate
from django.dispatch import receiver
from django.contrib.auth import get_user_model

class CustomUserManager(BaseUserManager):
    def create_user(self, username, password=None, **extra_fields):
        user = self.model(username=username, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Los superusuarios deben tener is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Los superusuarios deben tener is_superuser=True.')

        return self.create_user(username, password, **extra_fields)

class CustomUser(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(max_length=150, unique=True)
    password=models.CharField(max_length=50)
    first_name = models.CharField(max_length=30, blank=True)
    last_name = models.CharField(max_length=30, blank=True)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    date_joined = models.DateTimeField(default=timezone.now)

    objects = CustomUserManager()

    USERNAME_FIELD = 'username'

    def __str__(self):
        return self.username


class Tarea(models.Model):
    titulo = models.CharField(max_length=100)
    descripcion = models.TextField()
    COMPLETADA_CHOICES = [
        ('si', 'Si'),
        ('no', 'No'),
    ]
    completada = models.CharField(max_length=10, choices=COMPLETADA_CHOICES)
    user = models.ForeignKey('CustomUser', on_delete=models.CASCADE)
    fecha_creacion=models.DateTimeField(auto_now_add=True)
    def __str__(self) -> str:
        return self.username
    
@receiver(post_migrate)
def create_default_users(sender, **kwargs):
    User = get_user_model() 
    if sender.name == "appTareas":
        for user_data in [
            {'username': 'Diego', 'password': 'diego1234'},
            {'username': 'Juan', 'password': 'juan1234'},
            {'username': 'Manuel', 'password': 'manuel1234'},
            {'username': 'Laura', 'password': 'laura1234'},
            {'username': 'Sofia', 'password': 'sofia1234'},
        ]:
            user, created = User.objects.get_or_create(username=user_data['username'])
            if created:
                user.set_password(user_data['password'])
                user.save()
