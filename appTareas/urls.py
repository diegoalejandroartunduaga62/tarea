from django.urls import path
from appTareas import viewss
from .viewss import *

urlpatterns = [
    path('tareas/', viewss.TareaLista.as_view(), name='tarea-lista'),
    path('tareas/<int:pk>/', viewss.TareaDetalle.as_view(), name='tarea-detalle'),
    path('mis-tareas/', viewss.MisTareas.as_view(), name='mis-tareas'),
    path('lista-sin-terminar/', viewss.ListaSinTerminar.as_view(), name='lista-sin-terminar'),
    path('terminar-tarea/<int:pk>/', viewss.TerminarTarea.as_view(), name='terminar-tarea'),
    path('logout/', viewss.custom_logout_view, name='logout'),
    path('custom-login/', viewss.CustomLoginView.as_view(), name='custom-login'),
    path('custom-user-lista/', viewss.CustomUserLista.as_view(), name='custom-user-lista'),
    path('custom-user-lista/<int:pk>/', viewss.CustomUserDetalle.as_view(), name='custom-user-detalle'),
    path('already_authenticated/', viewss.already_authenticated_view, name='already_authenticated'),
    
    path('register/', RegisterUser.as_view()),
    path('Logins/', Logins.as_view()),
    path('registerTarea/', RegisterTarea.as_view(), name='register_tarea'),
]