from rest_framework import generics
from .models import Tarea
from .serializer import TareaSerializer
from .models import CustomUser
from .serializer import CustomUserSerializer
from django.contrib.auth import login,authenticate,logout
from django.contrib.auth.decorators import login_required
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from django.http import JsonResponse
from django.contrib.auth import get_user_model
import json
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.authtoken.models import Token
from django.utils import timezone
from django.http import JsonResponse
from rest_framework import viewsets,permissions
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated

def editar_tarea(request, tarea_id):
    tarea = get_object_or_404(Tarea, id=tarea_id)
    usuarios = get_user_model().objects.all()
    
    if request.method == 'POST':
        nuevo_titulo = request.POST['titulo']
        nueva_descripcion = request.POST['descripcion']
        nueva_completada = request.POST['completada']
        nuevo_asignado = request.POST['asignado']
        
        tarea.titulo = nuevo_titulo
        tarea.descripcion = nueva_descripcion
        tarea.completada = nueva_completada
        tarea.asignado = nuevo_asignado
        tarea.save()
        return HttpResponse('<script>alert("Tarea actualizada con éxito"); window.location="/tarea-lista/";</script>')
    return render(request, 'editar_tarea.html', {'tarea': tarea, 'usuarios': usuarios})


def eliminar_tarea(request, tarea_id):
    tarea = get_object_or_404(Tarea, id=tarea_id)
    
    if request.method == 'POST':
        tarea.delete()
        return JsonResponse({'mensaje': 'Tarea eliminada con éxito'})
    
    return render(request, 'eliminar_tarea.html', {'tarea': tarea})


def custom_logout_view(request):
    logout(request)
    return render(request, 'logout.html')

def cambiar_completada(request, tarea_id):
    if request.method == 'POST':
        tarea = Tarea.objects.get(id=tarea_id)
        nueva_completada = request.POST.get('nueva_completada', '')
        tarea.completada = nueva_completada
        tarea.save()
        return JsonResponse({'nueva_completada': nueva_completada})
    return JsonResponse({'error': 'Método no válido'}, status=400)

def login_view(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)

        if user is not None:
            login(request, user)
            data = {
                'message': 'Sesión iniciada',
                'redirect': '/tarea-lista/'
            }
            return HttpResponse(json.dumps(data), content_type="application/json")
        else:
            data = {
                'message': 'Credenciales incorrectas',
            }
            return HttpResponse(json.dumps(data), content_type="application/json")

    return render(request, 'login.html')

def already_authenticated_view(request):
    return render(request, 'already_authenticated.html')

def tarea_form_view(request):
    if request.method == 'POST':
        titulo = request.POST.get('titulo')
        descripcion = request.POST.get('descripcion')
        completada = request.POST.get('completada')
        asignado_id = request.POST.get('asignado')

        if asignado_id:
            asignado = CustomUser.objects.get(id=asignado_id)
        else:
            asignado = None

        fecha_creacion = timezone.now()

        tarea = Tarea(
            titulo=titulo,
            descripcion=descripcion,
            completada=completada,
            user=asignado,
            fecha_creacion=fecha_creacion 
        )
        tarea.save()

        return redirect('tarea-lista')
    usuarios = CustomUser.objects.all()
    return render(request, 'tarea_form.html', {'usuarios': usuarios})

def tarea_list_view(request):
    tareas = Tarea.objects.all()
    tareas = Tarea.objects.filter(user=request.user, completada="no")
    return render(request, 'tarea_list.html', {'tareas': tareas})

def tarea_list_complete_view(request):
    tareas = Tarea.objects.all()
    tareas = Tarea.objects.filter(user=request.user, completada="si")
    return render(request, 'tarea_listComplete.html', {'tareas': tareas})

class ListaSinTerminar(generics.ListCreateAPIView):
    serializer_class = TareaSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        user = self.request.user
        return Tarea.objects.filter(user=user, completada='no')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user, completada='no')

class TerminarTarea(generics.UpdateAPIView):
    serializer_class = TareaSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        user = self.request.user
        return Tarea.objects.filter(user=user)

    def perform_update(self, serializer):
        serializer.save(completada='si')

class RegisterTarea(APIView):
    http_method_names = ['get', 'post']
    def get(self, request):
        tareas = Tarea.objects.all()
        serializer = TareaSerializer(tareas, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
    def post(self, request):
        serializer = TareaSerializer(data=request.data)
        if serializer.is_valid():
            user_identifier = request.data.get('user')
            user = CustomUser.objects.filter(pk=user_identifier).first()
            if user:
                serializer.validated_data['user'] = user
                serializer.save()
                return Response({'status': 201, 'message': 'Tarea registrada exitosamente'}, status=status.HTTP_201_CREATED)
            else:
                return Response({'status': 400, 'message': 'CustomUser no encontrado'}, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({'status': 400, 'message': 'Faltan datos'}, status=status.HTTP_400_BAD_REQUEST)
        
class RegisterUser(APIView):
    http_method_names = ['get', 'post']
    def get(self, request):
        usuarios = CustomUser.objects.all()
        serializer = CustomUserSerializer(usuarios, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
    def post(self, request):
        serializer=CustomUserSerializer(data=request.data)
        if not serializer.is_valid():
            return Response({'status':403, 'errors':serializer.errors, 'messge':'Someting went wrong'})
        serializer.save()
        customuser=CustomUser.objects.get(username=serializer.data['username'])
        token_obj , _ =Token.objects.get_or_create(user=customuser)
        
        return Response({'status:':200, 'payload':serializer.data, 'token':str(token_obj),'messge':'tu informacion se guardo'})

class Logins(APIView):
    def post(self, request):
        username = request.data.get('username', None)
        password = request.data.get('password', None)

        if not username or not password:
            return Response({'status': 403, 'message': 'Usuario y contraseña requeridos'})

        try:
            customuser = get_user_model().objects.get(username=username)
        except get_user_model().DoesNotExist:
            return Response({'status': 403, 'message': 'Usuario no existe'})
        if not customuser.check_password(password):
            return Response({'status': 403, 'message': 'Contraseña incorrecta'})
        token_obj, created = Token.objects.get_or_create(user=customuser)
        return Response({'status': 200, 'payload': CustomUserSerializer(customuser).data, 'token': str(token_obj), 'message': 'Authenticacion Correcta'})
                
class CustomLoginView(APIView):
    def get(self, request):
        if request.user.is_authenticated:
            return render(request, 'already_authenticated.html')
        return render(request, 'login.html') 

    def post(self, request):
        username = request.data.get('username')
        password = request.data.get('password')

        user = authenticate(request, username=username, password=password)
        
        if user is not None:
            login(request, user)
            return Response({"detail": "Inicio de sesión exitoso."}, status=status.HTTP_200_OK)
        else:
            return Response({"detail": "Inicio de sesión fallido."}, status=status.HTTP_401_UNAUTHORIZED)


class MisTareas(generics.ListAPIView):
    def get_queryset(self):
        user = self.request.user
        return Tarea.objects.filter(user=user)

class CustomUserLista(generics.ListCreateAPIView):
    queryset = CustomUser.objects.all()
    serializer_class = CustomUserSerializer

class CustomUserDetalle(generics.RetrieveUpdateDestroyAPIView):
    queryset = CustomUser.objects.all()
    serializer_class = CustomUserSerializer

class TareaLista(generics.ListCreateAPIView):
    authentication_classes=[TokenAuthentication]
    permission_classes = [IsAuthenticated]
    queryset = Tarea.objects.all()
    serializer_class = TareaSerializer

class TareaDetalle(generics.RetrieveUpdateDestroyAPIView):
    queryset = Tarea.objects.all()
    serializer_class = TareaSerializer
    
class TareaViewSet(viewsets.ModelViewSet):
    authentication_classes=[TokenAuthentication]
    permission_classes = [IsAuthenticated]
    queryset=Tarea.objects.all()
    serializer_class=TareaSerializer
    
class TareaViewEdit(viewsets.ModelViewSet):
    # authentication_classes=[TokenAuthentication]
    # permission_classes = [IsAuthenticated]
    queryset=Tarea.objects.all()
    serializer_class=TareaSerializer
    
class LoginViewSet(viewsets.ModelViewSet):
    queryset=CustomUser.objects.all()
    serializer_class=CustomUserSerializer