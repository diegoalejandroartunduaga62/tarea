# Generated by Django 4.2.6 on 2023-11-01 20:32

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('appTareas', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='customuser',
            name='email',
        ),
    ]
